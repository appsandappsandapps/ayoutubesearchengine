package com.example.ayoutubesearchengine

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class YoutubeViewModel : ViewModel() {

  val videos = MutableStateFlow<List<String>>(listOf())

  fun fetch(
    searchTerm: String
  ) {

    viewModelScope.launch {
      val page = youtubeService.getSearchPage(searchTerm)
      val regex = Regex("""\/watch\?v=[^&\\u0026]*?\"""")
      val matches = regex.findAll(page)

      val videoIds = matches
        .map { it.value }
        .map { it.replace("""/watch?v=""", "")}
        .map { it.dropLast(1) }
        .toList()
      videos.value = videoIds
    }
  }

}
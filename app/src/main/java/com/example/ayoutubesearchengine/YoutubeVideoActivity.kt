package com.example.ayoutubesearchengine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class YoutubeVideoActivity : AppCompatActivity() {

  companion object {
    val BUNDLE_VIDEO_ID = "videoid"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.youtube_video)

    val videoId = intent.getStringExtra(BUNDLE_VIDEO_ID)

    val webview: WebView = findViewById(R.id.webview)
    setupWebView(webview)
    webview.loadUrl("https://www.youtube.com/embed/${videoId!!}?mute=0&autoplay=1")

  }

  private fun setupWebView(webview: WebView) {
    webview.settings.javaScriptEnabled = true
    webview.setWebChromeClient(WebChromeClient())
    webview.settings.pluginState = WebSettings.PluginState.ON
  }

}
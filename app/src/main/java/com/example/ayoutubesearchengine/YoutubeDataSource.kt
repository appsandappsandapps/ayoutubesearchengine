package com.example.ayoutubesearchengine

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

// Todo: Make into generic repository

interface YoutubeWebsite {
  @GET("/results")
  suspend fun getSearchPage(@Query("search_query") query: String):String
}

val youtubeService:YoutubeWebsite = Retrofit.Builder()
  .baseUrl("https://www.youtube.com")
  .addConverterFactory(ScalarsConverterFactory.create())
  .build()
  .create(YoutubeWebsite::class.java)

package com.example.ayoutubesearchengine

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import coil.load

class YoutubeRecyclerView(
  val videos: List<String>,
  val clickListener: (String) -> Unit,
) : RecyclerView.Adapter<YoutubeRecyclerView.Holder>() {
  
  override fun onCreateViewHolder(vg: ViewGroup, type: Int): Holder {
    val v = LayoutInflater.from(vg.context)
      .inflate(R.layout.youtube_list_item, vg, false)
    return Holder(v)
  }

  override fun onBindViewHolder(hld: Holder, pos: Int) {
    hld.image.load("https://img.youtube.com/vi/${videos[pos]}/mqdefault.jpg")
    hld.image.setOnClickListener {
      clickListener(videos[pos])
    }
  }

  override fun getItemCount(): Int = videos.size

  inner class Holder(v: View): RecyclerView.ViewHolder(v) {
    val image: ImageView
    init {
      image = v.findViewById(R.id.youtube_thumbnail)
    }
  }
}
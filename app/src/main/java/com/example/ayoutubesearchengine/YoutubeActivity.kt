package com.example.ayoutubesearchengine

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import androidx.recyclerview.widget.GridLayoutManager

class YoutubeActivity : AppCompatActivity() {

  lateinit var viewModel: YoutubeViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.youtube_list)
    viewModel = ViewModelProvider(this).get(YoutubeViewModel::class.java)
    waitForVideos()
    setupSearchBar()
  }

  private fun setupSearchBar() {
    findViewById<EditText>(R.id.youtube_search_term)
      .setOnEditorActionListener { textView, i, keyEvent ->
        if(textView.text.trim().length > 0) {
          viewModel.fetch(textView.text.toString())
          textView.clearFocus()
        }
        false
      }
  }

  private fun waitForVideos() {
    lifecycleScope.launch {
      //TODO: add on lifecycle callback
      viewModel.videos.collect {
        setupYoutubeListRecycler(it)
      }
    }
  }

  private fun setupYoutubeListRecycler(videos: List<String>) {
    val gridManager = GridLayoutManager(this, 3)
    findViewById<RecyclerView>(R.id.youtube_recyclerview)
      .apply {
        layoutManager = gridManager
        adapter = YoutubeRecyclerView(videos, { videoId ->
          openVideo(videoId)
        })
      }
  }

  private fun openVideo(videoId: String) {
    val intent = Intent(
      this,
      YoutubeVideoActivity::class.java
    ).apply {
      putExtra(YoutubeVideoActivity.BUNDLE_VIDEO_ID, videoId)
    }
    startActivity(intent)
  }
}